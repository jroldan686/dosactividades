package jrl.acdat.dosactividades;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Segunda extends AppCompatActivity implements View.OnClickListener {

    public static final String DATO = "nombre";
    Intent i;
    TextView miTexto;
    Button btnCerrar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_segunda);

        miTexto = (TextView)findViewById(R.id.textView2);
        btnCerrar = (Button)findViewById(R.id.button2);
        btnCerrar.setOnClickListener(this);

        i = this.getIntent();
        miTexto.setText("Hola " + i.getStringExtra(DATO));
    }

    @Override
    public void onClick(View v) {
        if(v == btnCerrar) {
            finish();
        }
    }
}
