package jrl.acdat.dosactividades;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    public static final String DATO = "nombre";
    Intent i;
    EditText miTexto;
    Button btnHola;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        miTexto = (EditText)findViewById(R.id.editText);
        btnHola = (Button)findViewById(R.id.button);
        btnHola.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(v == btnHola) {
            i = new Intent(this, Segunda.class);
            i.putExtra(DATO, miTexto.getText().toString());
            startActivity(i);
        }
    }
}
